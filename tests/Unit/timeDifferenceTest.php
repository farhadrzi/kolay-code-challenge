<?php

namespace Tests\Unit;


use App\Services\LeaveRightManagementService;
use App\Services\LeaveRightRuleManagementService;
use Tests\BaseTestApp;

class timeDifferenceTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_time_difference()
    {
        $classObject = new LeaveRightManagementService(new LeaveRightRuleManagementService());
        $class = self::getMethod(get_class($classObject), 'calculate_time_difference');
        $result = $class->invokeArgs($classObject, array('startDate'=>'19.03.2016','endDate'=>'29.07.2019'));
        $this->assertEquals(40,$result);
    }
}
