<?php

namespace Tests\Unit;

use App\Services\LeaveRightRuleManagementService;
use PHPUnit\Framework\TestCase;
use Tests\BaseTestApp;

class parseResponseCountryRuleTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_parse_response_country_rule()
    {
        $classObject = new LeaveRightRuleManagementService();
        $class = self::getMethod(get_class($classObject), 'call_country_rule_api');
        $response = $class->invokeArgs($classObject, array('countryCode'=>'tr'));

        $class = self::getMethod(get_class($classObject), 'parse_rule_api_response');
        $result = $class->invokeArgs($classObject, array('response'=>$response));
        $this->assertEquals('tr',$result->country_code);
    }
}
