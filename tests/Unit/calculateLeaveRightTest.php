<?php

namespace Tests\Unit;

use App\Services\LeaveRightManagementService;
use App\Services\LeaveRightRuleManagementService;
use PHPUnit\Framework\TestCase;

class calculateLeaveRightTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_calculate_leave_right()
    {
        $classObject = new LeaveRightManagementService(new LeaveRightRuleManagementService());
        $result = $classObject->calculate_leave_right('tr','19.03.2016','29.07.2019');
        $this->assertEquals(42, $result);
    }
}
