<?php

namespace Tests\Unit;

use App\Services\LeaveRightRuleManagementService;
use PHPUnit\Framework\TestCase;

class getCountryRuleTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_country_rule()
    {
        $classObject = new LeaveRightRuleManagementService();
        $result = $classObject->get_rule_from_api('tr');
        $this->assertEquals('tr', $result->country_code);
    }
}
