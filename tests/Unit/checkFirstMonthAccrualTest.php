<?php

namespace Tests\Unit;

use App\Services\LeaveRightManagementService;
use App\Services\LeaveRightRuleManagementService;
use PHPUnit\Framework\TestCase;
use Tests\BaseTestApp;

class checkFirstMonthAccrualTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_check_first_month_accrual()
    {
        $classObject = new LeaveRightManagementService(new LeaveRightRuleManagementService());
        $class = self::getMethod(get_class($classObject), 'calculate_month_first_accrual_months');
        $result = $class->invokeArgs($classObject, array('differenceMonth'=>40,'firstAccrualMonths'=>12));
        $this->assertEquals(true,$result);
    }
}
