<?php

namespace Tests\Unit;

use App\Services\LeaveRightRuleManagementService;
use Tests\BaseTestApp;

class callCountyRuleAPITest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_call_county_rule_api()
    {
        $classObject = new LeaveRightRuleManagementService();
        $class = self::getMethod(get_class($classObject), 'call_country_rule_api');
        $response = $class->invokeArgs($classObject, array('countryCode'=>'tr'));
        $this->assertTrue(str_contains($response, 'tr'));
    }
}
