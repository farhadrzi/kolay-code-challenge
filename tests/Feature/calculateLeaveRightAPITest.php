<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class calculateLeaveRightAPITest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_calculate_leave_right_api()
    {
        $response = $this->post('api/calculate/leaves/right',['countryCode'=>'tr','startDate'=>'19.03.2016','endDate'=>'29.07.2019']);

        $response->assertStatus(200);
    }
}
