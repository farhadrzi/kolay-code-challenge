<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class LeaveRightRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'countryCode'=>'required',
            'startDate'=>'required|date_format:d.m.Y',
            'endDate'=>'date_format:d.m.Y',
        ];
    }

    /**
     * Return specific message for every validation error
     * @return array|string[]
     */
    public function messages()
    {
        return [
            "countryCode.required"=>"please enter the country code",
            "startDate.required"=>"please enter your start date like this example : 19.03.2016",
            "startDate.date_format"=>"your date format is incorrect , please enter your start date like this example : 19.03.2016",
            "endDate.date_format"=>"your date format is incorrect , please enter your end date like this example : 19.03.2016"
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'messages' => $validator->getMessageBag()->first(),
        ], $status ?? 400,['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE));
    }
}
