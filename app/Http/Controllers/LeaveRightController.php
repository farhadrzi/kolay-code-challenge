<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeaveRightRequest;
use App\Services\LeaveRightManagementService;
use Exception;
use Illuminate\Http\Request;

class LeaveRightController extends AppBaseController
{
    /**
     * @var LeaveRightManagementService
     */
    private LeaveRightManagementService $leaveRightManagementService;

    public function __construct(LeaveRightManagementService $leaveRightManagementService)
    {
        parent::__construct();
        $this->leaveRightManagementService = $leaveRightManagementService;
    }

    /**
     * calculate leaves right days
     * @param LeaveRightRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculate_leave_right(LeaveRightRequest $request){
        try {
            $request->validated();

            $countryCode = $request->countryCode;
            $startDate = $request->startDate;
            $endDate = $request->endDate;

            $leaveBalance = $this->leaveRightManagementService->calculate_leave_right($countryCode, $startDate,$endDate);
            return $this->response->success($leaveBalance,'success');
        }catch (Exception $exception){
            return $this->response->error($exception->getMessage());
        }
    }
}
