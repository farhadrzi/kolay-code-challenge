<?php


namespace App\Services;


use Exception;

class LeaveRightRuleManagementService
{
    /**
     * get country rule base of each country code
     * @param $countryCode
     * @return mixed
     * @throws Exception
     */
    public function get_rule_from_api($countryCode){
        try {
            $response = $this->call_country_rule_api($countryCode);
            return $this->parse_rule_api_response($response);
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * call specific api for get data about country
     * @param $countryCode
     * @return string
     * @throws Exception
     */
    private function call_country_rule_api($countryCode):string{
        try {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://kolay-global-leave-policy.herokuapp.com/country/'.$countryCode,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            return $response;
        }catch (Exception $exception){
            throw new Exception('Error while call country rule API');
        }
    }

    /**
     *
     * parse response from string to json
     * @param string $response
     * @return mixed
     * @throws Exception
     */
    private function parse_rule_api_response(string $response){
        $json = json_decode($response);
        if(isset($json->error)){
            throw new Exception('country code not valid , please chose valid country code');
        }else{
            return  $json;
        }
    }
}
