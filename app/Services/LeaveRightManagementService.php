<?php


namespace App\Services;


use Carbon\Carbon;
use Exception;

class LeaveRightManagementService
{
    /**
     * @var LeaveRightRuleManagementService
     */
    private LeaveRightRuleManagementService $leaveRightRuleManagementService;

    public function __construct(LeaveRightRuleManagementService $leaveRightRuleManagementService)
    {
        $this->leaveRightRuleManagementService = $leaveRightRuleManagementService;
    }

    /**
     * calculate leaves right days
     * @param string $countryCode
     * @param string $startDate
     * @param null $endDate
     * @return int
     * @throws Exception
     */
    public function calculate_leave_right(string $countryCode,string $startDate,$endDate=null){
        try {
            $allowanceDay= 0;
            $countryRule = $this->leaveRightRuleManagementService->get_rule_from_api($countryCode);
            $differenceMonth = $this->calculate_time_difference($startDate,$endDate);
            if($this->calculate_month_first_accrual_months($differenceMonth, $countryRule->first_accrual_months)){

                for ($i=$countryRule->first_accrual_months;$i<=$differenceMonth;$i=$i+$countryRule->accrual_frequency_months){
                    $allowanceDay += $countryRule->accrual_amount_days/$countryRule->accrual_period_months;
                }

                return intval($allowanceDay*$countryRule->accrual_frequency_months);
            }else{
                return $allowanceDay;
            }
        }catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * calculate difference time between two specific time, if user dont enter end time this function select today as end time automatically
     * @param string $startDate
     * @param null $endDate
     * @return int
     * @throws Exception
     */
    private function calculate_time_difference(string $startDate,$endDate=null):int{
        if($endDate==null){
            $today = Carbon::createFromFormat('d.m.Y', now()->format('d.m.Y'));
        }else{
            $today = Carbon::createFromFormat('d.m.Y', $endDate);
        }
        $carbonStartDate = Carbon::parse($startDate)->format('d.m.Y');
        $difference = $today->diffInMonths($carbonStartDate);
        if($difference>=0){
            return $difference;
        }else{
            throw new Exception('the start date time must older than today');
        }
    }

    /**
     * check a employer work more than allowance month or not
     * @param $differenceMonth
     * @param $firstAccrualMonths
     * @return bool
     */
    private function calculate_month_first_accrual_months($differenceMonth,$firstAccrualMonths):bool{
        $remainMonth = $differenceMonth - $firstAccrualMonths;
        return $remainMonth>=0;
    }
}
